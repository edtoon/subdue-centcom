#!/bin/bash

set -e

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
SRC_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${SRC_DIR}/environment.cfg"
. "${SRC_DIR}/lib/read-machine-cfg.sh"

if [ -z "${1}" ];
then
  >&2 echo "Syntax: ${0} <machine-name>"
  exit 1
fi

read_machine_cfg ${1}

for project_name in ${PROJECTS}
do
  echo "Machine: ${MACHINE_NAME} building project: ${project_name}"
  docker-machine ssh "${MACHINE_FULL_NAME}" "cd /opt/${SYSTEM_NAME}/src/${project_name} && ./build.sh"
  echo "Machine: ${MACHINE_NAME} built project: ${project_name}"
done
