#!/bin/bash

set -e

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
SRC_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${SRC_DIR}/environment.cfg"
. "${SRC_DIR}/lib/read-machine-cfg.sh"

if [ -z "${1}" ];
then
  >&2 echo "Syntax: ${0} <machine-name>"
  exit 1
fi

read_machine_cfg ${1}

docker-machine ssh "${MACHINE_FULL_NAME}" "mkdir -p /opt/${SYSTEM_NAME}/src"

for project_name in ${PROJECTS}
do
  project_dir="${SRC_DIR}/../${project_name}"

  if [ -d "${project_dir}" ];
  then
    echo "Machine: ${MACHINE_NAME} rsyncing project: ${project_name}"
    chmod go-rwx "${MACHINE_STORAGE_PATH}/id_rsa"
    rsync -avzP \
      -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
      -o IdentitiesOnly=yes -o LogLevel=quiet \
      -i ${MACHINE_STORAGE_PATH}/id_rsa" --progress --delete \
      "${project_dir}/" "${MACHINE_USER}@${MACHINE_IP}:/opt/${SYSTEM_NAME}/src/${project_name}/"
    echo "Machine: ${MACHINE_NAME} rsynced project: ${project_name}"
  else
    >&2 echo "Couldn't find project dir: ${project_dir}"
  fi
done
