#!/bin/bash

set -e

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
SRC_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${SRC_DIR}/environment.cfg"
. "${SRC_DIR}/lib/read-machine-cfg.sh"

if [ -z "${1}" ];
then
  >&2 echo "Syntax: ${0} <machine-name>"
  exit 1
fi

read_machine_cfg ${1}

if [ -z "${MACHINE_IP}" ];
then
  echo "Machine: ${MACHINE_NAME} being created on DigitalOcean"

  if [ "digitalocean" = "${PROVIDER_NAME}" ];
  then
    read -s -p "Enter DigitalOcean access token: " DO_ACCESS_TOKEN
    docker-machine create --driver digitalocean \
      --digitalocean-access-token="${DO_ACCESS_TOKEN}" \
      --digitalocean-region "${DO_REGION}" \
      --digitalocean-size "${DO_SIZE}" \
      --digitalocean-image "ubuntu-14-04-x64" \
      "${MACHINE_FULL_NAME}"
  elif [ "virtualbox" = "${PROVIDER_NAME}" ];
  then
    docker-machine create --driver virtualbox "${MACHINE_FULL_NAME}"
  else
    >&2 echo "Unrecognized provider: ${PROVIDER_NAME}"
    exit 1
  fi

  docker-machine ssh "${MACHINE_FULL_NAME}" "sudo apt-get update -y && sudo apt-get install -y rsync"
  echo "Machine: ${MACHINE_NAME} was created on ${PROVIDER_NAME}"
else
  check_running=`docker-machine ls | grep "^${MACHINE_FULL_NAME} " | grep Running`

  if [ -z "${check_running}" ];
  then
    echo "Machine: ${MACHINE_NAME} starting"
    docker-machine start "${MACHINE_FULL_NAME}"
    echo "Machine: ${MACHINE_NAME} started"
  else
    echo "Machine: ${MACHINE_NAME} is already running"
    echo "${check_running}"
  fi
fi
