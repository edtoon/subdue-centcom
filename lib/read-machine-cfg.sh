#!/bin/bash

set -e

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
LIB_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${LIB_DIR}/determine-os.sh"

function read_machine_cfg
{
  if [ -z "${SYSTEM_NAME}}" ];
  then
    >&2 echo "read_machine_cfg requires SYSTEM_NAME"
    exit 1
  fi

  if [ -z "${1}" ];
  then
    >&2 echo "read_machine_cfg requires machine name param"
    exit 1
  fi

  local machine_cfg="${LIB_DIR}/../machines/${1}.cfg"

  if [ ! -e "${machine_cfg}" ];
  then
    >&2 echo "could not find machine config: ${machine_cfg}"
    exit 1
  fi

  . "${machine_cfg}"

  if [ -z "${PROVIDER_NAME}" ];
  then
    >&2 echo "config did not contain PROVIDER: ${machine_cfg}"
    exit 1
  fi

  MACHINE_NAME="${1}"
  MACHINE_FULL_NAME="${SYSTEM_NAME}-${PROVIDER_NAME}-${MACHINE_NAME}"

  if [ "digitalocean" = "${PROVIDER_NAME}" ];
  then
    MACHINE_USER="root"

    if [ -z "${DO_REGION}" ];
    then
      >&2 echo "config did not include DO_REGION: ${machine_cfg}"
    fi

    if [ -z "${DO_SIZE}" ];
    then
      >&2 echo "config did not include DO_SIZE: ${machine_cfg}"
    fi
  elif [ "virtualbox" = "${PROVIDER_NAME}" ];
  then
    MACHINE_USER="docker"
  else
    >&2 echo "Unrecognized provider: ${PROVIDER_NAME}"
    exit 1
  fi

  check_exists=`docker-machine ls -q | grep "^${MACHINE_FULL_NAME}\$" || true`

  if [ ! -z "${check_exists}" ];
  then
    MACHINE_IP=`docker-machine ip "${MACHINE_FULL_NAME}"`
    MACHINE_STORAGE_PATH=`docker-machine | grep -- "--storage-path" | cut -d '"' -f 2`"/machines/${MACHINE_FULL_NAME}"

    if [ "Cygwin" = "${OS}" ];
    then
      MACHINE_STORAGE_PATH=`cygpath -u "${MACHINE_STORAGE_PATH}"`
    fi
  fi
}
