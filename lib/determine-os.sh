#!/bin/bash

set -e

OS=$(uname -o 2>/dev/null || uname -s)

if [ "GNU/Linux" = "${OS}" ]; then
  OS=$(lsb_release -i -s)
fi
