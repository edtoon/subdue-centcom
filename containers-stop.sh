#!/bin/bash

set -e

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
SRC_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${SRC_DIR}/environment.cfg"
. "${SRC_DIR}/lib/read-machine-cfg.sh"

if [ -z "${1}" ];
then
  >&2 echo "Syntax: ${0} <machine-name>"
  exit 1
fi

read_machine_cfg ${1}

echo "Machine: ${MACHINE_NAME} stopping containers"
docker-machine ssh "${MACHINE_FULL_NAME}" "docker ps -q | xargs docker stop"
echo "Machine: ${MACHINE_NAME} stopped containers"
