#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
SRC_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${SRC_DIR}/environment.cfg"
. "${SRC_DIR}/lib/read-machine-cfg.sh"

if [ -z "${1}" ];
then
  >&2 echo "Syntax: ${0} <machine-name>"
  exit 1
fi

read_machine_cfg ${1}

if [ -z "${MACHINE_IP}" ];
then
  echo "Machine: ${MACHINE_FULL_NAME} is not launched"
else
  check_running=`docker-machine ls | grep "^${MACHINE_FULL_NAME} " | grep Running`

  if [ -z "${check_running}" ];
  then
    echo "Machine: ${MACHINE_NAME} is not running"
  else
    docker-machine ssh "${MACHINE_FULL_NAME}"
  fi
fi
